#ifndef VECT_H
#define VECT_H

#include "head.h"

template <class T>
class vect {
 public:
  vect();
  vect(int n1);

  void print(int i) { cout << "[" << i << "] " << vec[i] << endl; }
  void print(); 

  T& operator [] (int j);
  vect operator + (vect a);
  vect operator - (vect a);
  void operator * (double a);
  void operator / (double a);

  int getSize(){ return n; } 
  void append(T x);
  void add_to_pos(int pos, T value);
  void delete_last();

 private:
  T* vec; 
  int n;
  int max_size;
  void realloc(); // memory reallocation
};


template<class T>
vect<T>::vect()
{
  max_size = 1;
  n = 1;
  vec = new T[max_size];
  if(typeid(T) == typeid(float) || typeid(T) == typeid(double))
    for(int i = 0; i < n; i++) vec[i] = 0.0;
  else for(int i = 0; i < n; i++) vec[i] = 0;
}

template<class T>
vect<T>::vect(int n1)
{
  max_size = 2 * n1;
  n = n1;
  T *b = new T[max_size];
  vec = b;
  if(typeid(T) == typeid(float) || typeid(T) == typeid(double))
    for(int i = 0; i < n; i++) vec[i] = 0.0;
  else for(int i = 0; i < n; i++) vec[i] = 0;
}

template<class T>
void vect<T>::print() {
  cout << endl; 
  for (int i = 0; i < n; i++)
    cout << "[" << i << "] " << vec[i] << endl;
}

template<class T>
T& vect<T>::operator[](int j) {
  try {
    if(j < 0 || j >= n) throw 1;
    return vec[j];
  }
  catch(int i) {
    if(i == 1) cerr << "Index is out of bounds" << endl;
  }
}

//////////////////////////////////////////////////////
// arithmetic operators 
template <class T>
vect<T> vect<T>:: operator -(vect<T> a){
  vect c(n);
  for (int i = 0; i < n; i++){
    c.vec[i] = vec[i] - a.vec[i];
  }
  return c;
}


template <class T>
void vect<T>::operator *(double a){

  for (int i = 0; i < n; i++){
    vec[i] = vec[i] * a;
  }
}

template <class T>
void vect<T>::operator /(double a){

  for (int i = 0; i < n; i++){
    vec[i] = vec[i] / a;
  }
}

template <class T>
 vect<T> vect<T>:: operator +(vect<T> a){
   vect c(n);
   for (int i = 0; i < n; i++){
     c.vec[i] = vec[i] + a.vec[i];
   }
   return c;
};

/////////////////////////////////////////////
//add/delete element

template <class T>
void vect<T>::append(T x){
  vec[n] = x;
  n++;
  realloc();
}

template <class T>
void vect<T>::add_to_pos(int pos, T value) {

  for(int i = pos; i < n; i++)
    vec[i + 1] = vec[i];
  vec[pos] = value;

  realloc();
}

template <class T>
void vect<T>::delete_last(){
  n --;
  realloc();
}

/////////////////////////////////////////////////
//reallocation

template<class T>
void vect<T>::realloc() {
  if(n == max_size) {  // add memory
    max_size *= 2;
    T* b = new T[max_size];
    for(int i = 0; i < n; i++) {
      b[i] = vec[i];
    }
    delete[] vec;
    vec = b;
  }

  if(n < max_size / 4) { // delete memory
    max_size /= 2;
    T* b = new T[max_size];
    for(int i = 0; i < n; i++) {
      b[i] = vec[i];
    }
    delete[] vec;
    vec = b;
  }
}
//////////////////////////////////////////
#endif /* KAS_H */
