SRC = vect.cpp
OBJ = vect.o
TARGET = vect

.PHONY: all clean debug test

all: $(TARGET)

clean: 
	rm $(OBJ)

debug: $(SRC)
	g++ -g $(SRC) -o debug_file

vect.o: vect.cpp
	g++ -c vect.cpp -o vect.o

$(TARGET): $(OBJ)
	g++ $(OBJ) -o $(TARGET) 
